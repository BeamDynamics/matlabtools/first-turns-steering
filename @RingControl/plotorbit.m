function plotorbit(obj,varargin)
%PLOTTRAJECTORY display last measurement of trajectory
%
%see also: RingControl

p=inputParser;
addRequired(p,'obj');
addParameter(p,'figure',true,@islogical);
parse(p,obj,varargin{:});

if p.Results.figure
    figure;
end
if ~isempty(obj.last_measured_orbit)
    plot(obj.last_measured_orbit([1,2],:)');
    xlabel('BPM #')
    ylabel('[m]');
    yyaxis right
    plot(obj.last_measured_orbit(3,:)');
    ylabel('sum signal');
else
    disp('no orbit has been measured since initialization');
end