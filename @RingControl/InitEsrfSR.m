function InitEsrfSR(obj)
%SWITCHTOEBSSIMULATOR turns parameters of FirstTurns class to EBS
%simulator
%
% 
%
%see also: First2Turns

TANGO_HOST='tango://acs:10000/';

obj.MeasurementFolder = pwd;

obj.first_turn_index = 18;

%% movables

getfunction = @(devname)tango.Attribute(devname).value;
setfunction = @(devname,val)setfield(tango.Attribute(devname),'set',val);

% attributes to use. Defaults for: ESRF SR Aug 2018
obj.s12 = movable([TANGO_HOST 'sr/ps-si/12/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'calibration',43.8e-3 / 10e3,'absolute',true,'limits',[8500 9000].*(43.8e-3 / 10e3));
obj.s3 = movable([TANGO_HOST 'sr/ps-si/3/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'calibration',21.6431e-3 / 8e3,'absolute',true,'limits',[6200 6900].*(21.6431e-3 / 8e3));
%obj.cur_s12_set = 'sr/ps-si/12/Current';
%obj.cur_s3_set = 'sr/ps-si/3/Current';
obj.cv8 = movable([TANGO_HOST 'tl2/ps-c1/cv8/Current'],...
    'get_fun',getfunction,'set_fun',setfunction);
obj.cv9 = movable([TANGO_HOST 'tl2/ps-c1/cv9/Current'],...
    'get_fun',getfunction,'set_fun',setfunction);

obj.sh =  movable([TANGO_HOST 'srmag/hst/all/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'calibration',load_corcalib('sr',8),'limits',[-3e-4 3e-4]);
obj.sv =  movable([TANGO_HOST 'srmag/vst/all/Current'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'calibration',load_corcalib('sr',9),'limits',[-3e-4 3e-4]);

% limits can be higher, but they depend on the strengths of the steerers
obj.skew    = movable([TANGO_HOST 'srmag/sqp/all/CorrectionStrengths'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',true,'limits',[-1.5e-2 1.5e-2]);

obj.rf      = movable([TANGO_HOST 'srrf/master-oscillator/1/Frequency'],...
    'get_fun',getfunction,'set_fun',setfunction,...
    'absolute',false,'limits',[0.99 1.01]);

obj.quad    =  movable([TANGO_HOST 'srmag/m-q/all/CorrectionStrengths'],...
    'get_fun',getfunction,'set_fun',setfunction);

obj.sext    = movable([TANGO_HOST 'srmag/m-s/all/CorrectionStrengths'],...
    'get_fun',getfunction,'set_fun',setfunction);

obj.octu    = movable([TANGO_HOST 'srmag/m-o/all/CorrectionStrengths'],...
    'get_fun',getfunction,'set_fun',setfunction);

obj.initial_coordinates = movable([TANGO_HOST 'sys/ringsimulator/ebs/TbT_InCoord'],...
    'get_fun',getfunction,'set_fun',setfunction);



%% devices to use
obj.ke = [TANGO_HOST 'sy/ps-ke/1'];
obj.gun = [TANGO_HOST 'elin/beam/run'];
obj.rips = [TANGO_HOST 'sy/ps-rips/manager'];
obj.scraper = [TANGO_HOST 'sr/d-scr/c4-ext'];

%% diagnostics
obj.stored_current = @()tango.Attribute([TANGO_HOST 'sr/d-ct/1']).value;


%% function to measure trajectory, takes as input the class instance.
obj.getTbTdata = @(obj)getTbTdata_EBS_SIMU(obj); 
% inputs to function getTbTdata
obj.hor_bpm_TBT = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/TBT_HPositions']).value;
obj.ver_bpm_TBT = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/TBT_VPositions']).value;
obj.sum_bpm_TBT = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/TBT_Sums']).value;
obj.bpm_trigger_counter = @()tango.Attribute([TANGO_HOST 'sr/d-bpmlibera/c1-6/DDTriggerCounter']).value;
%load reference sum signal
% a=load('/users/beamdyn/dev/commissioningtools/@RingControl/referenceSRBPMsumsignal.mat','sumsignal');
% obj.sum_signal_beam = a.sumsignal; % sum signal with beam (sensitivity of each BPM)
% obj.sum_signal_background = zeros(size(a.sumsignal));
obj.sum_signal_beam = ones(1,320); % sum signal with beam (sensitivity of each BPM)
obj.sum_signal_background = zeros(1,320);
obj.sum_signal_threshold = 0.5e-6; % sum signal below this value is considered not good

%%  function to measure COD, takes as input the class instance.
obj.getSAdata = @(obj)getSAdata_EBS_SIMU(obj); 

% inputs to function getSAdata
obj.hor_bpm_SA = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/SA_HPositions']).value;
obj.ver_bpm_SA = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/SA_VPositions']).value;
obj.sum_bpm_SA = @()tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/SA_Sums']).value;

obj.status_bpm =@()(arrayfun(@(a)isequal(a,0),tango.Attribute([TANGO_HOST 'srdiag/beam-position/all/All_Status']).value)); % BPM status (0 means no error, so ok)
obj.state_hst =@()(strcmp(tango.Attribute([TANGO_HOST 'srmag/hst/all/CorrectorStates']).value,'Disabled')==0); % H steerer status (ON OFF FAULT,...)
obj.state_vst =@()(strcmp(tango.Attribute([TANGO_HOST 'srmag/vst/all/CorrectorStates']).value,'Disabled')==0); % V steerer status (ON OFF FAULT,...)

% index of correctors to exclude [0,n-1]
obj.orbit_cor_disabled_bpm =@()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-h/DisabledBPMsIndex']).value); % H steerer not in orbit correction
obj.orbit_cor_disabled_hst =@()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-h/DisabledActuatorsIndex']).value); % H steerer not in orbit correction
obj.orbit_cor_disabled_vst =@()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-v/DisabledActuatorsIndex']).value); % V steerer not in orbit correction

obj.autocor =@()(tango.Attribute([TANGO_HOST 'sr/beam-orbitcor/svd-auto']).state);

%% other usefull devices

obj.stored_current = @()tango.Attribute([TANGO_HOST 'srdiag/beam-current/total/Current']).value;
obj.KillBeamAtGivenCurrent = @(obj)KillBeamAtGivenCurrentESRF(obj);

obj.h_tune = @()tango.Attribute([TANGO_HOST 'srdiag/beam-tune/1/Tune_h']).value;
obj.v_tune = @()tango.Attribute([TANGO_HOST 'srdiag/beam-tune/1/Tune_v']).value;
obj.h_emittance = @()tango.Attribute([TANGO_HOST 'srdiag/emittance/id07/Emittance_h']).value;
obj.v_emittance = @()tango.Attribute([TANGO_HOST 'srdiag/emittance/id07/Emittance_v']).value;
obj.lifetime = @()tango.Attribute([TANGO_HOST 'srdiag/beam-current/total/Lifetime']).value;
obj.neutrons = '';


%% lattice
kick = [...
    0.000489845903684   0.000426168812966   0.000426168820758   0.000489845969593;... -1 mm
    0.002451840946955   0.002131227999229   0.002131228196820   0.002451842601757;... -5 mm
    0.004910225944962   0.004263696516143   0.004263697305621   0.004910232596004;... -10 mm
    0.007375181085586   0.006397873450655   0.006397875223752   0.007375196110139]; % -15 mm

% - 10mm kickers bump
K3 = 0.0;% kick(3,3);
K4 = 0.0;% kick(3,4);

obj.kick = kick;
obj.K3 = K3;
obj.K4 = K4;
obj.injoff = -0.0060; % set simulator TbT input to [injoff, 0,0,0,0,0]

mod = ebs.model('reduce',true,'keep','QF8D');
ring = mod.ring;
obj.indBPM = mod.get(0,'bpm')';
obj.indHst = sort([mod.get(0,'steerh'); mod.get(0,'dq')])';
obj.indVst = mod.get(0,'steerv')';
obj.indSkew = mod.get(0,'skew')';

% make sure steerers have KickAngle.
ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,1},0);
ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,2},0);
ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,1},0);
ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,2},0);

% quadrupole indexes (BAD FOR QF8D of 2PW cells)
qpind = mod.get(0,'qp');
qf8dind = mod.get(0,'qf8d');
obj.indQuad=sort([qpind; qf8dind(1:50:end)])';

obj.indSext = mod.get(0,'sx')';
obj.indOctu = mod.get(0,'oc')';

ring=atsetcavity(atradon(ring),6.5e6,1,992);
obj.rmodel = ring;

obj.tl2model ={};

tl2file = '/operation/beamdyn/matlab/optics/tl2/theory/betamodel.mat';
b = load(tl2file,'betamodel');
obj.tl2model = b.TL2;

end
