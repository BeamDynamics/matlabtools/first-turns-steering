function InitEsrfSY(obj)

TANGO_HOST='';

obj.first_turn_index = 14;

obj.MeasurementFolder = pwd; %'/mntdirect/_machfs/liuzzo/EBS/Simulator/FirstTurn';


%% devices
% for automated injection
obj.ke = '';% 'sy/ps-ke/1'; %% missing
obj.gun = [TANGO_HOST 'elin/beam/run'];% 'elin/beam/run';%% missing
obj.rips = [TANGO_HOST 'sy/ps-rips/manager'];% 'sy/ps-rips/manager';%% missing
obj.scraper = '';% 'sr/d-scr/c4-ext';%% missing


%% movables
% septa and CV not in simulator
obj.s12 = movable('');% 'sr/ps-si/12/CurrentSetPoint';%% missing
obj.s3 = movable('');% 'sr/ps-si/3/CurrentSetPoint';%% missing
obj.s12_set = movable('');% 'sr/ps-si/12/Current';%% missing
obj.s3_set = movable('');% 'sr/ps-si/3/Current';%% missing
obj.cv8 = movable('');% 'tl2/ps-c1/cv8/Current';%% missing
obj.cv9 = movable('');% 'tl2/ps-c1/cv9/Current';%% missing

obj.sh = movable([TANGO_HOST 'sy/st-h/all/Current'],'limits',[-3e-4 3e-4]);
obj.sv = movable([TANGO_HOST 'sy/st-v/all/Current'],'limits',[-3e-4 3e-4]);
obj.rf = movable([TANGO_HOST '']);

obj.quad = movable('');
obj.sext = movable('');
obj.octu = movable('');


%% diagnostics
obj.hor_bpm_TBT = [TANGO_HOST 'sy/d-bpmlibera-sp/all/XPosition'];
obj.ver_bpm_TBT = [TANGO_HOST 'sy/d-bpmlibera-sp/all/ZPosition'];
obj.sum_bpm_TBT = [TANGO_HOST 'sy/d-bpmlibera-sp/all/Sum'];% 'sr/d-bpmlibera/all/Sum_DD';  %% missing

obj.hor_bpm_SA = [TANGO_HOST 'sy/d-bpmlibera-sp/all/XPosition'];
obj.ver_bpm_SA = [TANGO_HOST 'sy/d-bpmlibera-sp/all/ZPosition'];
obj.sum_bpm_SA =[];

obj.bpm_trigger_counter = [TANGO_HOST 'sy/d-bpmlibera-sp/all/FillCounter'];

obj.status_bpm =[TANGO_HOST 'sy/d-bpmlibera-sp/all/All_Status']; % BPM status (0 means no error, so ok)
obj.state_hst =[TANGO_HOST 'sy/st-h/all/SteererStates']; % H steerer status (ON OFF FAULT,...)
obj.state_vst =[TANGO_HOST 'sy/st-v/all/SteererStates']; % V steerer status (ON OFF FAULT,...)
obj.orbit_cor_disabled_hst =[TANGO_HOST 'sy/svd-orbitcor/h/DisabledActuatorsIndex']; % H steerer not in orbit correction
obj.orbit_cor_disabled_vst =[TANGO_HOST 'sy/svd-orbitcor/v/DisabledActuatorsIndex']; % V steerer not in orbit correction
obj.autocor ='';

%load reference sum signal
obj.sum_signal_beam = []; % sum signal with beam (sensitivity of each BPM)
obj.sum_signal_background = [];

obj.store_current = [TANGO_HOST 'sy/d-ct/1/Current']; 


obj.h_tune = '';
obj.v_tune = '';
obj.h_emittance = '';
obj.v_emittance = '';
obj.lifetime = '';
obj.neutrons = '';

%% lattice
kick = [0.0 0.0 0.0 0.0]; % -15 mm

% - 10mm kickers bump
K3 = 0.0;% kick(3,3);
K4 = 0.0;% kick(3,4);

obj.kick = kick;
obj.K3 = K3;
obj.K4 = K4;
obj.injoff = -0.0; % set simulator TbT input to [injoff, 0,0,0,0,0]

% load('/users/beamdyn/dev/commissioningtools/@RingControl/Booster.mat'); % ring lattice
% ring = b;
% indBPM = find(atgetcells(ring,'FamName','BPM'))';
% indHCor = find(atgetcells(ring,'FamName','CH\w*'))';
% indVCor = find(atgetcells(ring,'FamName','CV\w*'))';
% obj.indBPM=indBPM;
% obj.indHst=indHCor;
% obj.indVst=indVCor;
% obj.indQuad=findcells(ring,'Class','Quadrupole');

mod = sy.model; % get lattice from default location (/operation/appdata/sy/optics/settings/theory/betamodel.mat)
ring = mod.ring;
obj.indBPM = mod.get(0,'bpm');
obj.indHst = mod.get(0,'steerh');
obj.indVst = mod.get(0,'steerv');
obj.indQuad = mod.get(0,'qp');
obj.indSext = mod.get(0,'sx');
obj.indOctu = [];

ring = atsetfieldvalues(ring,[indHCor,indVCor],'PassMethod','CorrectorPass');

obj.rmodel = ring;
obj.TrajFileName = '';
obj.current = 0;
obj.radiants = 1;
obj.calibsh_ = load_corcalib('sy','h')*ones(1,39);
obj.calibsv_ = load_corcalib('sy','v')*ones(1,39);
obj.calibcv89_ = 1.0;
obj.calibs12_ = 1.0;
obj.calibs3_ = 1.0;



end 