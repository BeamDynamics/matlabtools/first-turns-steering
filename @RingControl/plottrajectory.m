function plottrajectory(obj,varargin)
%PLOTTRAJECTORY display last measurement of trajectory
%
%see also: RingControl

p=inputParser;
addRequired(p,'obj');
addParameter(p,'figure',true,@islogical);
parse(p,obj,varargin{:});

if p.Results.figure
    figure;
end

% estimate tunes from trajetory
obj.estimatetune;
mufft = obj.phase_advance_measured;
mumodel = obj.phase_advance_expected;

Qfft = obj.full_tune_measured;
Qmodel = obj.full_tune_expected;

plot(obj.last_measured_trajectory([1,2],:)','LineWidth',3);
xlabel('BPM #')
ylabel('[m]');
ax =gca;
ax.FontSize =14;
ax.Color='None';
ax.XLim=[0,obj.lastbpm];
ax.Title.String={['\Delta Q_{expected-measured}: ' num2str(Qmodel - Qfft,'%2.2f  ')];...
    ['measured \mu@BPM' num2str(obj.lastbpm,'%d') ': ' num2str(mufft,'%2.2f  ')];...
    ['expected \mu@BPM' num2str(obj.lastbpm,'%d') ': ' num2str(mumodel,'%2.2f  ')];...
    ['']};
yyaxis right
plot(obj.last_measured_trajectory(3,:)','LineWidth',3);
ylabel('sum signal');
grid on;
ax =gca;
ax.FontSize =14;
ax.Color='None';
ax.XLim=[0,obj.lastbpm];
f = gcf;
f.Color=[1 1 1];