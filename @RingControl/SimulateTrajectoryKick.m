function t = SimulateTrajectoryKick(obj,indSt,kick,plane)
%SIMULATETRAJECTORY simulates the trajectory on a perfect machine with a
%   kick on a steerer
%
%  t = SimulateTrajectoryKick(indSt,kick)
%  t    2x(Nbpmx2) array of  trajectory (hor and vert)
%  
%  indSt is the number of the steerer that you want to power
%  kick is the angle in radians you want to apply to the steerer
%
%  The simulation is done with linepass. 
%  steerer is horizonal or vertical depending on obj.plane
%  number of turns simulated is obj.nturns
%  disabled bpms (obj.getEnabledBPM) give nan as trajectory
%
% see also: linepass, getEnabledBPM

bpm_enabled=obj.getEnabledBPM;

r=obj.rmodel;
if strcmp(plane,'H')
    r{obj.indHst(indSt)}.KickAngle=[kick,0];
elseif strcmp(plane,'V')
    r{obj.indVst(indSt)}.KickAngle=[0, kick];
else
    error('plane must be ''H'' or ''V''');
end
rmany=repmat(r,obj.nturns,1);
bpm_enabled_many=repmat(bpm_enabled,1,obj.nturns);
indbpm_many=[];
for ii=1:obj.nturns
    indbpm_many=[indbpm_many, (obj.indBPM+(ii-1)*length(r))];
end

rin=[obj.injoff;0;0;0;0;0];
traj=linepass(rmany,rin,indbpm_many);

t=zeros(2,length(obj.indBPM)*obj.nturns);
t(1,:)=traj(1,:);
t(1,~bpm_enabled_many)=nan;

t(2,:)=traj(3,:);
t(2,~bpm_enabled_many)=nan;
t(2,isnan(t(1,:)))=nan;


end

