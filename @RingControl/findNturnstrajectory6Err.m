function [t,injNturns,refpos,tl2kick] = findNturnstrajectory6Err(...
    obj,ring,K,V,H,inCOD)
%[t    6x(Nbpmx2) array of  trajectory   
% ] = find2turnstrajectory6Err( 
% obj,...
% ring,.. 1) ring ( must be a separated input to be able to modify it)
% K,      2) kickers strengths K3 K4
% V,      3) CV89 strenghts for tl2 (if tl2 not empty)
% H,      4) Septa 2, 3 strenghts for tl2 (if tl2 not empty)
% iCOD ) 5) 6x1 input coordinates (default: zero)
%
%
% 
% uses linepass to obtain trajectory in ring at indBPM
% if present, BPM errors are included on x(1st) and y(3rd) coordinates.
% 
% obj.injoff = -15 mm 
% obj.K3 obj.K4 set for -10mm bump,
% beam will oscillate by 5mm at injection
% 
%see also: linepass bpm_matrices bpm_process

if nargin<6
    inCOD=[0 0 0 0 0 0]';
end

Nturns = obj.nturns;

tl2 = obj.tl2model;
injoff = obj.injoff;
indBPM = obj.indBPM;

% injected beam offset compared to center of chamber in SR
injectionoffset=atmarker('InjOff');
injectionoffset.T1=[0 0 0 0 0 0]';
injectionoffset.T2=[injoff 0 0 0 0 0]';

% SR injection bump
indK3K4 = find(atgetcells(ring,'FamName','DR_K[34]'))'; % icker idexes
if ~isempty(indK3K4)
    ringkickers = atsetfieldvalues(ring,indK3K4,'PassMethod','StrMPoleSymplectic4Pass');
    ringkickers = atsetfieldvalues(ringkickers,indK3K4(1),'PolynomB',{1,1},K(1));
    ringkickers = atsetfieldvalues(ringkickers,indK3K4(2),'PolynomB',{1,1},K(2));
else
    ringkickers = ring;
end

% remove S3 aperture restriction
indS3 = find(atgetcells(ringkickers,'FamName','S3_Septum'))'; % septum idexes
ringkickers = atsetfieldvalues(ringkickers,(indS3-1:indS3),'RApertures',{1,1},-50e-3);

% TL2 steering
if ~isempty(tl2)
    ind = find(atgetcells(tl2,'FamName','TL2_S2','TL2_S3'));
    tl2kick = atsetfieldvalues(tl2,ind(1),'PolynomB',{1,1},H(1));
    tl2kick = atsetfieldvalues(tl2kick,ind(2),'PolynomB',{1,1},H(2));
    
    ind = find(atgetcells(tl2,'FamName','TL2_VS_08','TL2_VS_09'));
    tl2kick = atsetfieldvalues(tl2kick,ind(1),'PolynomA',{1,1},V(1));
    tl2kick = atsetfieldvalues(tl2kick,ind(2),'PolynomA',{1,1},V(2));
else
    tl2kick = {};
end
% put together full trajectory simulation:
% TL2 (CV8, CV9), 
% 1st turns with kickers on, 
% 2nd turn kickers off
%
injNturns = [tl2kick;{injectionoffset};ringkickers;repmat(ring,Nturns,1)];
refpos = [];

for it = 1:Nturns
    refpos = [refpos length(tl2)+indBPM+length(ring)*(it-1)]; %index of BPMs
end

% % % % look at whole trajectory
% if ~isempty(tl2kick)
%     outtr=linepass(tl2kick,inCOD,1:length(tl2kick));
%     s = findspos(tl2kick,1:length(tl2kick));
%     figure; plot(s,outtr(1,:));
%     hold on; plot(s,outtr(3,:));
% end
% outtr=linepass(injNturns,inCOD,1:length(injNturns));
% s = findspos(injNturns,1:length(injNturns));
% figure; plot(s,outtr(1,:));
% hold on; plot(s,outtr(3,:));

% linepass
outtr=linepass(injNturns,inCOD,refpos);
ox=outtr(1,:);
oy=outtr(3,:);

% set bpm errors
[rel,tel,trand] = bpm_matrices(injNturns(refpos));
bpmreading = bpm_process([ox; oy],rel,tel,trand);
t=outtr;
t(1,:)=bpmreading(1,:);
t(3,:)=bpmreading(2,:);

end

