function dq = computecorrection(obj,svd_mode,varargin)
%method of class RingControl 
% 
% switch between orbit correction methods: 
% 'e','eigenvectors' svd inversion with limited number of eigenvectors 
% 't','Tikhonov' svd inversion with Tikhonov regularization
% 'b','BestCor' BestCor single most effective corrector
%
%
%see also: @RingControl.qemsvd @RingControl.qemsvd_Tikhonov
%@RingControl.BestCor

switch svd_mode
    case {'e','eigenvectors'}
        dq = obj.qemsvd(varargin{:});
    case {'t','Tikhonov'}
        dq = obj.qemsvd_Tikhonov(varargin{:});
    case {'b','BestCor'}
        [dq,ibest] = obj.BestCor(varargin{1},varargin{2});
        
        switch obj.machine
            case 'ebs-simu'
                disp(['Best cor: ' ebs.steername(ibest) ' ' num2str(dq(ibest)./obj.sh.calibration(ibest)) ' A']);
            case 'esrf-sr'
                disp(['Best cor: ' sr.steername(ibest) ' ' num2str(dq(ibest)./obj.sh.calibration(ibest)) ' A']);
            case 'esrf-sy'
                disp(['Best cor: ' sy.steername(ibest) ' ' num2str(dq(ibest)./obj.sh.calibration(ibest)) ' A']);
            otherwise
                disp(['Best cor: ' num2str(ibest) ' ' num2str(dq(ibest)./obj.sh.calibration(ibest)) ' A']);
        end

    otherwise
        dq = obj.qemsvd(varargin{:});
end

end

