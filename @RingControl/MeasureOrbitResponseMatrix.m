function [h,v,h2v,v2h,rfh,rfv] = MeasureOrbitResponseMatrix(obj,varargin)
% function to measure orbit response matrix
%
% this is a method of class @RingControl
%
%  INPUT (variable):
% 'Hsteerers' : vector of indexes in steerer list or mask of boolean
%               these are the steerers used for RM measurement.
%               default = 1:(TOT/32*2):TOT  (1 every two cells)
% 'Vsteerers' : vector of indexes in steerer list or mask of boolean
%               these are the steerers used for RM measurement.
%               default = 1:(TOT/32*2):TOT  (1 every two cells)
% 'Frequency' : boolean, measure or not RF response. default: true
% 'TwoSide'   : boolean, compute - kick and + kick. default: true
% 'delta'     : 3x1 vector of variations for H,V steerers and RF.
%               [rad, rad, Hz]. default [1e-5 1e-5 50]
% 'ignoreinch': ignore BPM inchoerency. default: false
% 'full'      : if first argument then measure FULL all correctrors and  RF
%               response matrix. default: false
% 'threshold_orbit_drifting' :  maximum accepted std of difference between
%               measurement of orbit after response and initial one. (to
%               chck if steerers whent back to their nominal setpoint).
% 'algorithm' : BPM algorithm. default = 0. 1 for MAAF.
%
% OUTPUT:
% h           : horizontal response to horizontal kick.
% v           : vertical response to vertical kick.
% h2v         : vertical response to horizontal kick.
% v2h         : horizontal response to vertical kick.
%
% also stores PINHOLES response
% (for graphic display of emittance evolution at pinholes)
%
% the function generates files:
% ----- for steerers response with the following format
%
% ----- for RF response with the following format
%
% ----- for pinhole response with the following format
%
% full RM file in cvs format. (in case full rm is measured this file is
% used by srco application)
%
% if the measurement is interrupted the data is still available and a next
% call will not repeat the measurement already done if files with the
% correct name are found.
%
%
% Example of usage
% >> rc = RingControl('ebs-simu');
% >> rc.MeasureOrbitResponseMatrix;
%
% or (FULL response):
% >> rc.MeasureOrbitResponseMatrix('full');
%
%
% to restore correctors values at the moment of RingControl initializations
% >> rc.sv.restore_initial_values
% >> rc.sh.restore_initial_values
%
%see also: ebs.model, sy.model, tl2.model, @RingControl, respmat.py

%% get initial informations
additional_wait_time = 5;

measurementtime = datestr(now);

%initial settings:
h0 = obj.sh.get;
v0 = obj.sv.get;
rf0 = obj.rf.get;
NHall = length(h0);
NVall = length(v0);

%% parse inputs
p = inputParser;

addOptional(p,'full',false,@islogical)
addParameter(p,'Hsteerers',1:(NHall/32*2):NHall,@(x)(isnumeric(x) | islogical(x)));
addParameter(p,'Vsteerers',1:(NVall/32*2):NVall,@(x)(isnumeric(x) | islogical(x)));
addParameter(p,'Frequency',true,@islogical);
addParameter(p,'TwoSide',true,@islogical);
addParameter(p,'delta',[5e-5 5e-5 150],@(x)(isnumeric(x) & length(x)==3));
addParameter(p,'ignoreinch',false);
addParameter(p,'num_acquisitions',obj.n_acquisitions);
addParameter(p,'threshold_orbit_drifting',1e-4);
addParameter(p,'algorithm',0);
addParameter(p,'workingdirectory',pwd);

parse(p,varargin{:});

full        = p.Results.full;
hlist       = p.Results.Hsteerers;
vlist       = p.Results.Vsteerers;
RF          = p.Results.Frequency;
twoside     = p.Results.TwoSide;
delta       = p.Results.delta;
deltah  = delta(1);
deltav  = delta(2);
deltarf = delta(3);
ignoreinch  = p.Results.ignoreinch;
nacq        = p.Results.num_acquisitions;
threshold_orbit_drifting = p.Results.threshold_orbit_drifting;
algo        = p.Results.algorithm;

workingdirectory = p.Results.workingdirectory;


if islogical(hlist)
    hlist=find(hlist);
end
if islogical(vlist)
    vlist=find(vlist);
end

if full
    disp('FULL RM requested, all steerers H/V and RF are used. Be very patient.')
    hlist =1:NHall;
    vlist =1:NVall;
    RF = true;
end


switch obj.machine
    case 'ebs-simu'
        hnames = ebs.hsteername(hlist','hst-');
        vnames = ebs.vsteername(vlist');
        
    case 'esrf-sr'
        hnames = sr.steername(hlist');
        vnames = sr.steername(vlist');
        
        
        %
        %     case 'esrf-sy'
        %
        %     case 'esrf-tl2'
        
    otherwise
        error(['Orbit Reponse Matrix not defined for ' obj.machine]);
end

NH = length(hlist);
NV = length(vlist);

%% initialize measurements

% CHECK AUTOCOR IS NOT ON.
autocor=tango.Device(obj.autocor);
if autocor.State ~= 'Off' %#ok<BDSCA>
    error('Autcor is not OFF');
end

% pinhole emittances before measurement
pinholes_list = ebs.pinholename(1:5);
for iph =  1:length(pinholes_list)
    try
        phdev_list{iph} = tango.Device(pinholes_list{iph}); %#ok<AGROW> % get devices
    catch err
        warning(['impossible to get device pinhole: ' pinholes_list{iph}])
        disp(err)
    end
end

% function to loop trough cameras measurement. will be replaced by
% srdiag/beam-emittance/all/Emittance_H (?)
    function [ex,ey]=readpinholes(doplot)
        ey = NaN(size(pinholes_list));
        ex = NaN(size(pinholes_list));
        
        for ip = 1:length(pinholes_list)
            try
                ph = phdev_list{ip};
                ex(ip) = ph.Emittance_h.read;
                ey(ip) = ph.Emittance_v.read;
            catch errpin
                disp(['impossible to read device pinhole: ' pinholes_list{ip}])
                %disp(errpin)
            end
        end
        
        if doplot
            figure;
            bar([ex;ey]');
            bp = gca;
            legend('hor','ver');
            bp.XTick=1:length(pinholes_list);
            bp.XTickLabels=pinholes_list;
            bp.XTickLabelRotation = 45;
        end
    end


[ex0,ey0]=readpinholes(true); %#ok<ASGLU>
save(fullfile(workingdirectory,['ApparentEmittances' measurementtime]),'pinholes_list','ex0','ey0');
iax_values=ey0;
save(fullfile(workingdirectory,['iaxemittance']),'iax_values');


% initial orbit
na0 = obj.n_acquisitions;
obj.n_acquisitions = nacq;

% orbit measurement. returns [bpm hor.;bpm ver.].
measfun=@()obj.measureorbit('ignoreinch',ignoreinch,'algorithm',algo);
o0 = measfun();  % reference orbit measurement.
NBPM = length(o0(1,:));

%% define nested functions. here we make use of variables available from the enclosing function.
% check for orbit drifting. to be used after each steerer movement,
% to check if orbit back to nominal.
    function [ok,st,mx] = checkorbit(o,ref)
        % get orbit sum, std, maximum
        if ~isempty(ref)
            o = o([1,2],:)- ref([1,2],:);
        else
            o = o([1,2],:);
        end
        ok = sum2(isfinite(o),2);
        st = std2(o,0,2);
        mx = max(abs(o),2)';
    end

    function driftcontrol()
        % measure current orbit and compare to intial reference
        %
        
        disp('check orbit drift compared to reference measurement before RM'); pause(2);
        
        ocheck = measfun();  % reference orbit measurement.
        
        [~, st, ~ ] = checkorbit(ocheck,o0);
        if any(st > threshold_orbit_drifting)
            error(['Orbit is drifting. std(COD-REF) (h,v,sum)'...
                num2str(st') ' ,'...
                'Probably one steerer did not return to its nominal value or something else is moving.'])
        end
    end


    function response = measure(mode,variation,actuatorindex)
        % subfunction for generic orbit measurement
        %
        % 'mode' may be:
        % 'H' : horizontal rsponse
        % 'V' : vertical response
        % 'F' : frequency response
        %
        % actuatorindex : steerer index. for mode F it is not used.
        %
        % returns NOT normalized response.
        
        disab = false;
        
        % get individual device attribute
        switch mode
            case 'H'
                A = obj.sh;% moovable class object instance
                
                filename = ['steer' mode num2str(hlist(actuatorindex),'%.3d')];
                hvlist=hlist;
                
            case 'V'
                A = obj.sv;% moovable class object instance
                
                filename = ['steer' mode num2str(vlist(actuatorindex),'%.3d')];
                hvlist=vlist;
                
                
            case 'F'
                A = obj.rf; % moovable class object instance
                
                filename = ['steer' 'F00' ];
                hvlist=1;
                actuatorindex = 1; % only one RF frequency.
                
        end
        
        
        % check if file already exist, in case, skip.
        if exist(fullfile(workingdirectory,[filename '.mat']),'file')
            disp(['File ' filename ' already exists. Skip measurement']);
            a = load(fullfile(workingdirectory,[filename,'.mat']));
            response=a.response;
            try
                real_variation=a.real_variation;
            catch
                real_variation = variation;
            end
           %  filenametotxt;
            return
        end
        
        % check if it is enabled
        switch mode
            case 'H'
                
                en = obj.getEnabledHsteerers;
                if en(hlist(actuatorindex))
                    disp(['Measuring ' hnames{actuatorindex} ...
                        ' ' num2str(actuatorindex) ' / ' num2str(length(hnames))])
                else % if not enabled skip measurement
                    warning([hnames{actuatorindex} ' is not enabled '...
                        '(either in orbit correction or itself). SKIPPED'])
                    response = zeros(size(o0));
                    real_variation = 1;
                    disab = true;
                end
                
            case 'V'
                
                en = obj.getEnabledVsteerers;
                if en(vlist(actuatorindex))
                    disp(['Measuring ' vnames{actuatorindex} ...
                        ' ' num2str(actuatorindex) ' / ' num2str(length(vnames))])
                else % if not enabled skip measurement
                    warning([vnames{actuatorindex} ' is not enabled '...
                        '(either in orbit correction or itself). SKIPPED'])
                    response = NaN(size(o0));
                    real_variation = 1;
                    disab = true;
                end
                
            case 'F'
                
                disp('RF response')
                
        end
        
        if ~disab
            % initial value of actuator
            K0 = A.get;
            DKp = zeros(size(K0)); % initialize array of real variation
            DKm = zeros(size(K0));
            
            if twoside % double sided measurement
                % up
                Kp=K0;
                Kp(hvlist(actuatorindex)) = K0(hvlist(actuatorindex)) + 0.5*variation;
                disp(['UP: ' num2str(K0(hvlist(actuatorindex)),'%3.2e') ...
                    ' + ' num2str(0.5*variation,'%3.2e') ...
                    ' = ' num2str(Kp(hvlist(actuatorindex)),'%3.2e')]);
                A.set(Kp); % moovable set also waits for setpoint reach.
                
                % get real variation in case it could not reach set point
                pause(additional_wait_time)
                DKp = A.get - K0;
                disp(['Asked variation of: ' num2str(0.5*variation) ...
                    ' got ' num2str(DKp(hvlist(actuatorindex)))]);
                
                op = measfun();
                disp(['Dorbit: ' num2str(std2(op'-o0'))]);
                disp(['+ orbit rms: ' num2str(std2(op([1,2],:)'))]);
                
                % down
                Km=K0;
                Km(hvlist(actuatorindex)) = K0(hvlist(actuatorindex)) - 0.5*variation;
                disp(['DOWN: ' num2str(K0(hvlist(actuatorindex)),'%3.2e') ...
                    ' - ' num2str(0.5*variation,'%3.2e') ...
                    ' = ' num2str(Km(hvlist(actuatorindex)),'%3.2e')]);
                A.set(Km);% moovable set also waits for setpoint reach.
                % get real variation in case it could not reach set point
                pause(additional_wait_time)
                DKm = A.get - K0;
                disp(['Asked variation of: ' num2str(-0.5*variation) ...
                    ' got ' num2str(DKm(hvlist(actuatorindex)))]);
                
                om = measfun();
                disp(['Dorbit: ' num2str(std2(om'-o0'))]);
                disp(['- orbit rms: ' num2str(std2(om([1,2],:)'))]);
                
            else % single sided
                Kp=K0;
                Kp(hvlist(actuatorindex)) = K0(hvlist(actuatorindex)) + variation;
                disp(['UP: ' num2str(K0(hvlist(actuatorindex)),'%3.2e') ...
                    ' + ' num2str(variation,'%3.2e') ...
                    ' = ' num2str(Kp(hvlist(actuatorindex)),'%3.2e')]);
                A.set(Kp);% moovable set also waits for setpoint reach.
                
                % get rea variation
                pause(additional_wait_time)
                DKp = A.get - K0;
                disp(['Asked variation of: ' num2str(0.5*variation) ...
                    ' got ' num2str(DKp(hvlist(actuatorindex)))]);
                
                op = measfun();
                disp(['Dorbit: ' num2str(std2(op'-o0'))]);
                disp(['+ orbit rms: ' num2str(std2(op([1,2],:)'))]);
                
                om = o0;
                DKm = K0 - K0;
                
            end
            
            % return to intial value
            A.set(K0);
            pause(additional_wait_time)
            
            % compute response (not normalized)
            response = (op-om)  ;
            
            real_variation = DKp(hvlist(actuatorindex)) - DKm(hvlist(actuatorindex));
            
            % check that orbit is back to its place.
            driftcontrol; % this function returns an ERROR if orbit changed befor and after measurement
        end
        
            % get pinholes sizes variations
            [ex,ey]=readpinholes(false); %#ok<ASGLU>
            
            % save file also if disabled during measurement (all zeros are stored)
            %matlab
            save(fullfile(workingdirectory,filename),'response','variation','real_variation','mode','ex','ey','ex0','ey0');
            % text
            filenametotxt;
        
        function filenametotxt
            %text (same file names and format of respmat.py
            switch mode
                case 'H'
                    fh = fopen([filename ''],'w');
                    fv = fopen([strrep(filename,'H','H2V') ''],'w');
                    devname = hnames{actuatorindex};
                case 'V'
                    fh = fopen([strrep(filename,'V','V2H') ''],'w');
                    fv = fopen([filename ''],'w');
                    devname = vnames{actuatorindex};
                case 'F'
                    fh = fopen([strrep(filename,'F','F2H') ''],'w');
                    fv = fopen([strrep(filename,'F','F2V') ''],'w');
                    devname = A.attr_name;
            end
            
            
            fprintf(fh,['%.12f\t%s,%.1d\t' repmat('%.12f\t',1,NBPM-1) '%.12f'],...
                real_variation,devname,algo,response(1,:));
            fclose(fh);
            fprintf(fv,['%.12f\t%s,%.1d\t' repmat('%.12f\t',1,NBPM-1) '%.12f'],...
                real_variation,devname,algo,response(2,:));
            fclose(fv);
        end
        
    end

%% measurement
% initialize Response matrices
h = NaN(NBPM,NH);
v = NaN(NBPM,NV);

rfh = NaN(NBPM,1);
rfv = NaN(NBPM,1);

h2v = NaN(NBPM,NH); % horizontal steerer to vertical orbit
v2h = NaN(NBPM,NV); % vertical steerer to horizontal orbit

% for plots
hplot = h;
vplot = v;

rfhplot = rfh;
rfvplot = rfv;

h2vplot = h2v; % horizontal steerer to vertical orbit
v2hplot = v2h; % vertical steerer to horizontal orbit



% figure for response matrix evolution display.
figure('units','normalized','Position',[0.05,0.2,0.9,0.6]);
% horizontal
subplot(2,1,1);
axH = gca;
stdh = std2([h, rfh]);
stdv = std2([h2v,rfv]);
stdvalues =[ stdh ; stdv]';
bH=bar(axH,stdvalues);
axH.XLim = [0, NH+2];
axH.XTick = 1:length(hlist)+1;
axH.XTickLabel=[hnames;'frequency'];%obj.rf.attr_name];
axH.XTickLabelRotation = 90;
axH.Title.String= 'horizontal steerers response';
axH.YLabel.String = 'std of orbit response [mm]';
legend('hor.','ver');
hold on;
bH(1).YDataSource ='stdh';
bH(2).YDataSource ='stdv';

% vertical
subplot(2,1,2);
axV = gca;
stdh = std2(v2h);
stdv = std2(v);
stdvalues =[ stdh ; stdv]';
bv=bar(axV,stdvalues);
axV.XLim = [0, NV+1];
axV.XTick = 1:length(vlist);
axV.XTickLabel=vnames;
axV.XTickLabelRotation = 90;
axV.Title.String= 'vertical steerers response';
axV.YLabel.String = 'std of orbit response [mm]';
legend('hor.','ver');
hold on;
bv(1).YDataSource ='stdh';
bv(2).YDataSource ='stdv';

% horizontal response matrix
disp(['Measure Orbit RM for ' num2str(length(hlist)) ' / ' num2str(NHall) ' horizontal correctors']);

for ii = 1:length(hlist)
    try
        % measure
        resp = measure('H',deltah,ii);
        
        % assign in global RM
        h(:,ii) = resp(1,:)'./deltah;
        h2v(:,ii) = resp(2,:)'./deltah;
        hplot(:,ii) = resp(1,:)'*1e3;
        h2vplot(:,ii) = resp(2,:)'*1e3;
        
        % plot
        stdh = std2([hplot, rfhplot]);
        stdv = std2([h2vplot,rfvplot]);
        refreshdata(axH,'caller');
        drawnow;
        
    catch err
        disp(err);
        error(['Error evaluating response for ' hnames{ii}]);
    end
end

if RF
    disp(['Measure Orbit RM for RF frequency. Initial f_RF: ' num2str(rf0,'%9.2f') ' Hz']);
    try
        % measure
        resp = measure('F',deltarf,1);
        
        % assign in global RM
        rfh(:,1) = resp(1,:)'./deltarf;
        rfv(:,1) = resp(2,:)'./deltarf;
        rfhplot(:,1) = resp(1,:)'*1e3;
        rfvplot(:,1) = resp(2,:)'*1e3;
        
        % plot
        stdh = std2([hplot,rfhplot]);
        stdv = std2([h2vplot,rfvplot]);
        refreshdata(axH,'caller');
        drawnow;
        
    catch err
        disp(err);
        error(['Error evaluating Frequency response']);
    end
end

% figure for response matrix evolution display.
clear resp;

disp(['Measure Orbit RM for ' num2str(length(vlist)) ' / ' num2str(NVall) ' vertical correctors']);
for ii = 1:length(vlist)
    try
        % measure
        resp = measure('V',deltav,ii);
        
        % assign in global RM
        v2h(:,ii) = resp(1,:)'./deltav;
        v(:,ii) = resp(2,:)'./deltav;
        v2hplot(:,ii) = resp(1,:)'*1e3;
        vplot(:,ii) = resp(2,:)'*1e3;
        
        % plot
        stdh = std2(v2hplot);
        stdv = std2(vplot);
        refreshdata(axV,'caller');
        drawnow;
        
    catch err
        disp(err);
        error(['Error evaluating response for ' vnames{ii}]);
    end
end

save(fullfile(workingdirectory,['OrbitResponseMatrix' measurementtime '.mat']),...
    'h','v','h2v','v2h','rfh','rfv');
disp('End of measurement');

% csv files for operation
switch obj.machine
    case 'ebs-simu'
        if NH == NHall && NV == NVall
            ebs.autocor_model(obj.rmodel,'exp',pwd);
        else
            warning('CSV file not created, partial measurement only.')
        end
    otherwise
end

% restore ringcontrol object to initial state.
obj.n_acquisitions = na0;

end
