function [DGL,GL0,GLf,quadDevice]=movetune(Q1,Q0,varargin)
%function [DGL,GL0,GLf,quadDevice]=movetune(Q1,Q0,varargin)
%
% returns currents to move SR tunes to Q1 starting from Q0
% 
% INPUTS:
%
% Required
% Q1: [Qx,Qy] wished tune (measured)
% Q0: [Qx,Qy] initial tune. default is the measured tunes
%
% Optional (any order, string, value, string, value, ...) 
% 'mode' : 'QF1QD2'  : set tune using main families
%          'Matching' : move tune and keep optics locked
% 'lattice' : measured RM folder to execute qemextract (not yet there) or
%             AT lattice
%             (default: '', uses model lattice) 
% 'setcur'  : true/false(default), sets currents in the SR 
%
% OUTPUTS:
% DGL: delta quadruple integrated strengths
% GL0: initial quadruple integrated strengths for Q0
% GLf: final quadrupole  integrated strengths for Q1
% quadDevice: tango devices names 
%
% example 1): default usage 
% [DGL,GL0,GLf]=movetune([.20,.40],[.21 .34]);
%
% example 2): change mode and use measured lattice 
% (not yet done)
% 
%see also: atfittune

hostname=getenv('TANGO_HOST');%['Tango://ebs-simu:10000/'];

% PROBLEM HERE. ADDPATH, functions needed not in +ebs 
setenv('TANGO_HOST', hostname);

% parse inputs
p = inputParser;

expectedmode={'QF1QD2','Matching'};
defaultmode=expectedmode{1};

defaultlattice='';% no errors lattice
defaultsetcur=false;% no setting of currents

% PROBLEM HERE. defaults from tango.Device
% dd=tango.Device([hostname 'srdiag/beam-tune/1']);
% defaulttunetocorrect=[dd.Tune_h.read dd.Tune_v.read];

addRequired(p,'Q1',@isnumeric);
addRequired(p,'Q0',@isnumeric);
addOptional(p,'mode',defaultmode,@(x) any(validatestring(x,expectedmode)));
addOptional(p,'lattice',defaultlattice,@(x)(isstr(x) || iscell(x)));
addOptional(p,'setcur',defaultsetcur);

parse(p,Q1,Q0,varargin{:});

changetunemode        = p.Results.mode;
lattice     = p.Results.lattice;
setcur      = p.Results.setcur;

disp(repmat('-',1,20))
disp(['Tunes from: ' num2str(Q0)])
disp(['        to: ' num2str(Q1)])
disp(repmat('-',1,20))

% use lattice model if matching requested
if strcmp(changetunemode,expectedmode([2]))
    disp('Matching mode. Using model lattice without errors.')
    disp(['lattice: ' lattice ' will be ignored.'])
    lattice='';
end


% load lattice
if isempty(lattice)
    sr=ebs.model();
    r=sr.ring;
elseif iscell(lattice)
    r=lattice;
    sr = ebs.model(r);
else
    error('not implemented yet. not possible to use measured lattice model')
    sr=ebs.model('/machfs/MDT/2020/Dec...');
    r=sr.ring;
end

% [resp,~]=ebs.tuneresponse(sr,changetunemode);
% DGL = resp*(Q1-Q0)';

latmodel = sr;
tunemode = changetunemode;
% get quadurpole and DQ indexes
iq  = latmodel.get(0,'qp');    % quadrupoles 
iq8  = latmodel.get(0,'qf8d'); % tilted qf8d (varying dipole component, 50 slices)
idq = latmodel.get(0,'dq');    % dipole quadrupole (fixed dipole component, but may be splitted for markers)
allq_unsort = [iq;iq8;idq];
% get order of quadrupoles components in lattice
[allq,iqa] = sort(allq_unsort); 

% get lattice with initial tunes
latinitunes=ebs.model(latmodel);%,'reduce',true);
latinitunes.settune(Q0,tunemode);

% get lattice with final tunes
latnewtunes=ebs.model(latmodel);%,'reduce',true);
latnewtunes.settune(Q1,tunemode);

% compute difference in gradients
DGL_=latnewtunes.get(1,'qp') - latinitunes.get(1,'qp');
D8GL=latnewtunes.get(1,'qf8d') - latinitunes.get(1,'qf8d');
DqGL=latnewtunes.get(1,'dq') - latinitunes.get(1,'dq');
DGL=[DGL_; D8GL; DqGL];

q_group = NaN(size(allq));
    
ii=1;
ig=1;
while ii<length(allq)
    
    q_group(ii)=ig;
    % if not subsequent or after subsequent (mid. marker), change group 
    if allq(ii+1)-allq(ii)~=1 && allq(ii+1)-allq(ii)~=2
        ig=ig+1;
    end
    
    ii=ii+1;
    
end
Nq= q_group(end-1)+1;
q_group(end) = Nq;

function dgls=sumk(dgl,qg)
        % sum integrated gradient change for magnets of the same group
        % (slices)
        % dgl are the delta gradient, qg the quadrupole number, both 
        % in lattice order.
        nq=max(qg);
        dgls=zeros(nq,1);
        
        for inq=1:nq
            dgls(inq)=sum(dgl(qg==inq));% sum gradients
        end
        
end

DGLsum = sumk(DGL(iqa),q_group)';


% get present strengths
quadDevice = 'srmag/m-q/all';
q = tango.Device(quadDevice);
GL0 = q.CorrectionStrengths.read;
GLf = GL0 + DGLsum;


if setcur
    save('InitialQuadCorrectionStrengths','GL0');
    % q.CorrectionStrenghts = GLf;
    
    % set currents slowly.
    p=GLf';
    pact=GL0';
    
    maxchange=0.01; p=GLf';
  
    
    if max(abs(p-pact))>maxchange
        
        [pmax]=max(abs(p-pact));
        
        NP=floor(pmax/maxchange)+1;
        
        disp(['large excursion, sharing in ' num2str(NP) ' steps'])
        
        ptemp=pact;
        
        %     disp('initial values')
        %         disp(pact')
        %     disp('final values')
        %         disp(p')
        %
        for stepset=1:NP
            ptemp=ptemp+(p-pact)./(NP);
            
            %         disp(['current values, step ' num2str(stepset) '/' num2str(NP)])
            %         disp(ptemp')
            %
            
            q.CorrectionStrengths = ptemp';
            
            pause(0.5);
        end
        
        q.CorrectionStrengths = p';
        
    else
        % set in a singe shot
        q.CorrectionStrengths = p';
    end
    
end

end
