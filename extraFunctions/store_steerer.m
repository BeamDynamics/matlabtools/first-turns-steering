function store_steerer(fname,st,st_name)

fid=fopen(fname,'w');
for i=1:size(st,1)
fprintf(fid,'%s\t%.6f\t%.4f\t%.6f\t%.5e\n',st_name{i},st(i,1:4));
end
fclose(fid);
