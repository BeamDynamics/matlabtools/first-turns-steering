function y = var2(x,w,dim)
%VAR2 Variance. Ignores NaNs
%   For vectors, VAR(X) returns the variance of X.  
%   For matrices, VAR(X) is a row vector containing the
%   variance of each column of X. 
%
%   VAR2(X) normalizes by N where N is the sequence length. VAR2(X) is
%   the second moment of the sample about its mean.
%
%   VAR2(X,0) normalizes by N-1 where N is the sequence length.  This
%   makes VAR(X,0) the best unbiased estimate of the variance if X
%   is a sample from a normal distribution.
%
%   VAR2(X,1) normalizes by N and produces the second moment of the
%   sample about its mean.
%
%   VAR2(X,W) computes the variance using the weight vector, W.
%   The number of elements in W must equal the number of rows 
%   in X unless W = 1 which is treated as a short-cut for a 
%   vector of ones.
%
%   The elements of W must be positive. VAR2 normalizes W by
%   dividing each element in W by the sum of all its elements.
%
%   The variance is the square of the standard deviation
%   (STD2). 
%
%   See also MEAN2, STD2, COV and CORRCOEF.
%
%   The variance is the square of the standard deviation (STD).
%
%   Example: If X = [4 -2 1
%                    9  5 7]
%      then var(X,0,1) is [12.5 24.5 18.0] and var(X,0,2) is [9.0
%                                                             4.0]
%
%   Class support for inputs X, W:
%      float: double, single
%
%   See also MEAN, STD, COV, CORRCOEF.

%   VAR supports both common definitions of variance.  If X is a
%   vector, then
%
%      VAR(X) = SUM(RESID.*CONJ(RESID)) / (N-1)
%      VAR(X,1) = SUM(RESID.*CONJ(RESID)) / N
%
%   where RESID = X - MEAN(X) and N is LENGTH(X).
%
%   The weighted variance for a vector X is defined as
%
%      VAR(X,W) = SUM(W.*RESID.*CONJ(RESID)) / SUM(W)
%
%   where now RESID is computed using a weighted mean.

%   Copyright 1984-2004 The MathWorks, Inc.
%   $Revision: 1.7.4.4 $  $Date: 2004/03/09 16:16:33 $

if nargin < 2, w = 1;
elseif isempty(w), w = 1;
end

if nargin < 3
    % The output size for [] is a special case when DIM is not given.
    if isequal(x,[]), y = NaN(class(x)); return; end

    % Figure out which dimension sum will work along.
    dim=min(find(size(x) ~= 1));
    if isempty(dim), dim = 1; end
end

% Will replicate out the mean of X to the same size as X.
n=size(x,dim);
tile = ones(1,max(ndims(x),dim)); tile(dim) = n;

    % The unbiased estimator: divide by (n-1).
if isequal(w,0)
    nok=sum(isfinite(x), dim);
    x0 = x - repmat(mdiv(sum2(x, dim),nok), tile);
    y = mdiv(sum2(abs(x0).^2, dim),(nok - 1));

    % The biased estimator: divide by n.
elseif isequal(w,1)
    nok=sum(isfinite(x), dim);
    x0 = x - repmat(mdiv(sum2(x, dim),nok), tile);
    y = mdiv(sum2(abs(x0).^2, dim),nok);

    % Weighted variance
else
    dw=size(w);
    if (length(dw) > 2) | all(dw > 1) | numel(w) == 1
       error('MATLAB:var:invalidWgts','W must be a vector of nonnegative weights, or a scalar 0 or 1.');
    elseif numel(w) ~= n
        error('MATLAB:var:invalidSizeWgts','The length of W must be compatible with X.');
    elseif any(w < 0)
        error('MATLAB:var:invalidWgts','W must be a vector of nonnegative weights, or a scalar 0 or 1.');
    end

    % Normalize W, and embed it in the right number of dims.  Then
    % replicate it out along the non-working dims to match X's size.
    wtile = size(x); if dim <= ndims(x), wtile(dim) = 1; end
    w = repmat(reshape(w ./ sum(w), tile), wtile);

    x0 = x - repmat(sum2(w .* x, dim), tile);
    y = sum2(w .* abs(x0).^2, dim); % abs guarantees a real result
end

function y=mdiv(a,b)
y=NaN*ones(size(a));
ok=(b~=0);
y(ok)=a(ok)./b(ok);
