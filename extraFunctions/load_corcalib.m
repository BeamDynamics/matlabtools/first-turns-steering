function varargout=load_corcalib(varargin)
%LOAD_CORCALIB			loads corrector calibration
%
%[H,V,S,Q]=LOAD_CORCALIB(OPTICS,...);
%
%The OPTICS argument may be one of the following:
%
%- 'sy', 'ebs'
%
%H,V : hor./vert. steerer calibration in radians/A
%
%[COEF1,...]=LOAD_CORCALIB(MACH,PLANE1,...) returns the selected calibs
%
%PLANE : 'h' for horizontal steerers	[1x1]
%        'v' for horizontal steerers	[1x1]
%        's' for skew quad correctors	[32x1]
%        'q' for normal quad correctors	[1x1]
%
%

mach = varargin{1};

if strcmp(mach,'ebs')				% default values
   disp('no calibrations');
elseif strcmp(mach,'sy')
    disp('Using default calibration');
    h=3.79587e-3;
    v=3.93469e-3;
    q=[];
    s=[];
end

varargout={h,v,s,q};
