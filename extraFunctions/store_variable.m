function store_variable(fname,name,value)
%STORE_VARIABLE     Adds a variable to a .mat file
%
%STORE_VARIABLE(FILENAME,VARNAME,VALUE)
%
%FILENAME:  Name of a .mat file on the MATLAB path
%VARNAME:   Variable name
%VALUE:     Value of the variable

try
    v=load(fname);
catch %#ok<CTCH>
    v=struct;
end
v.(name)=value; %#ok<STRNU>
save(fname,'-struct','v');
