function [varargout] = celldeal(cellv)
%CELLDEAL Splits a cell array in separate variables
%
%The last output variable contains the rest of the input cell array

lg=length(cellv);
if nargout < lg
    varargout=[cellv(1:nargout-1) {cellv(nargout:end)}]; %#ok<VARARG>
else
    varargout=[cellv repmat({''},1,nargout-lg)]; %#ok<VARARG>
end

end

