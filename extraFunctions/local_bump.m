function [kicks,eff,in,orbit]=local_bump(resp,sb,sk,columns,ampl)
%[kicks,eff,orbit]=LOCAL_BUMP(resp,sb,sk,kindex) computes local bumps
%	resp: response matrix
%	sb: BPM positions
%	sk: kicker positions
%	kindex: vector of 3 steerer indexes

if columns(1) > columns(3)
   out=sb<sk(columns(1)) & sb>sk(columns(3));
   in=[find(sb>sk(columns(1))) ; find(sb<sk(columns(3)))];
else
   out=sb<sk(columns(1)) | sb>sk(columns(3));
   in=find( sb>sk(columns(1)) & sb<sk(columns(3)) );
end

out=find( out(:) & all(isfinite(resp'))' );
kicks=[1;-resp(out,columns(2:end))\resp(out,columns(1))];
eff=resp(in,columns)*kicks;

if nargin >= 5
   best=-sort(-eff);
   scale=ampl/best(1);
   kicks=kicks*scale;
   eff=eff*scale;
end
if nargout >= 4
   orbit=resp(:,columns)*kicks;
%  error=std(orbit(out));
end
