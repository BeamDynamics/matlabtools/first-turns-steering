% RUN THIS EXAMPLE FOR A TEST OF FIRST2TURNS Class usage.

ft = FirstTurns('ebs-simu');

% measure one trajectory
ft.measuretrajectory;

figure; 
plot(ft.last_measured_trajectory'); 
xlabel('BPM #'); ylabel('[m]');
legend('hor','ver');

% get actual corrector values
hst0 = ft.getHSteerersValues;
vst0 = ft.getVSteerersValues;

figure;
bar([hst0;vst0]);
xlabel('Steerer #'); ylabel('[m]');
legend('hor','ver');

t0 = ft.findNturnstrajectory6Err(ft.rmodel,[0,0],[0,0],[0,0]);
figure; plot(t0');
ft.setTurns(50);
t0 = ft.findNturnstrajectory6Err(ft.rmodel,[0,0],[0,0],[0,0]);
figure; plot(t0');
ft.setTurns(2);


% perform correction (uncomment to test)
% ft.correction
