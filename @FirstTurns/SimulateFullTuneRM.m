function FullTuneRM= SimulateFullTuneRM(obj,varargin)
% SimulateFullTuneRM
% 
% optional name-value inptus:
% 'number_of_units', (1) : W.P. + -nu:1:nu  in hor. and ver. are tested
% 'kickh', 1e-4 : kick for trajectory difference in H
% 'kickv', 1e-4 : kick for trajectory difference in V
% 
% changes tune by +/- nu units compared to nominal and stores simulated
% trajectory response. 
% 
% 
  
r0 = obj.rmodel; % ring lattice

%% parse inputs
p = inputParser();
addRequired(p,'obj');
addOptional(p,'number_of_units',1,@isnumeric);
addOptional(p,'kickh',1e-4,@isnumeric);
addOptional(p,'kickv',1e-4,@isnumeric);

parse(p,obj,varargin{:});

inCOD            = zeros(6,1);
nu               = p.Results.number_of_units;
kickh            = p.Results.kickh;
kickv            = p.Results.kickv;

K = [0,0];
CV = [0,0];
Septa = [0,0];

disp('Simulated full tune RM computation in progress ... ')

l = atlinopt(r0,0,1:length(r0)+1);
w0 = l(end).mu/2/pi; % full tunes

kh0 = atgetfieldvalues(r0,obj.indHst(1),'PolynomB',{1,1});
kv0 = atgetfieldvalues(r0,obj.indVst(1),'PolynomA',{1,1});
       
Lh = r0{obj.indHst(1)}.Length;
Lv = r0{obj.indVst(1)}.Length;
       
% range of units to scan
nu = -nu:1:nu;

for it = 1:length(nu)
    
    % move horizontal tune by n units
    wh(it,:) = w0+[nu(it) 0];
    r = atfittune(r0,wh(it,:),'QF1\w*','QD2\w*','UseIntegerPart');
    r = atfittune(r,wh(it,:),'QF1\w*','QD2\w*','UseIntegerPart');
    r = atfittune(r,wh(it,:),'QF1\w*','QD2\w*','UseIntegerPart');
    l = atlinopt(r,0,1:length(r)+1);
    wh(it,:) = l(end).mu/2/pi; % full tunes
    disp(['got h tune: ' num2str(wh(it,:)) ' instead of:' num2str(w0+[nu(it) 0])]);
    
    [th0] = obj.findNturnstrajectory6Err(r,K,CV,Septa); % reference trajectory
    
    rh = atsetfieldvalues(r,obj.indHst(1),'PolynomB',{1,1},kh0+kickh/Lh);
    
    [th] = obj.findNturnstrajectory6Err(rh,K,CV,Septa); % difference trajectory with kick H (septum)

    % compute trajectory response to horizontal kick
    TH(it,:) = (th(1,:)-th0(1,:))./kickh;   
    
    % move vertical tune by n units
    wv(it,:) = w0+[0 nu(it)];
    r = atfittune(r0,wv(it,:),'QF1\w*','QD2\w*','UseIntegerPart');
    r = atfittune(r,wv(it,:),'QF1\w*','QD2\w*','UseIntegerPart');
    r = atfittune(r,wv(it,:),'QF1\w*','QD2\w*','UseIntegerPart');
    l = atlinopt(r,0,1:length(r)+1);
    wv(it,:) = l(end).mu/2/pi; % full tunes 
    disp(['got v tune: ' num2str(wv(it,:)) ' instead of:' num2str(w0+[0.0 nu(it)])]);
    
    % compute trajectory response to vertical kick
    [tv0] = obj.findNturnstrajectory6Err(r,K,CV,Septa); % reference trajectory
    
    rv = atsetfieldvalues(r,obj.indVst(1),'PolynomA',{1,1},kv0+(kickv/Lv));
    
    [tv] = obj.findNturnstrajectory6Err(rv,K,CV,Septa); % difference trajectory with kick H (septum)

    % compute trajectory response to horizontal kick
    TV(it,:) = (tv(3,:)-tv0(3,:))./kickv;   
    
end

FullTuneRM.QH = wh;
FullTuneRM.QV = wv;
FullTuneRM.TH = TH;
FullTuneRM.TV = TV;

obj.FullTuneRM = FullTuneRM;

disp('Simulated full tune RM finished, FullTuneRM updated ');

