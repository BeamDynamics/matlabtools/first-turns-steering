function C = findrespmatNturns(obj, PERTURB, PVALUE, FIELD, M,N, varargin)
%findrespmatNturns computes the change in the closed orbit due to parameter perturbations
%
% 2 sided matrix is computed (Oplus-Ominus)/2
%
% INPUTS:
% PERTURB, index of elements to modify
% PVALUE, value of perturbation to apply
% FIELD, field to perturb (ex: 'KickAngle')
% M,N index in FIELD
%
% Optiona INPUTS:
% 'K',value     : 1x2 double, K3 K4 strenghts
% 'CV',value    : 1x2 double, CV8 Cv9 strenghts
% 'Septa',value : 1x2 double, S12 S3 strenghts
%
% OUTPUTS:
% Returns a 1-by-4 cell array of O-by-P matrixes
% where O = length(OBSINDEX) and P = length(PERTURB)
% one for each of the close orbit components: X, PX, Y, PY
% C cell array 1x4:
%   C{1} hor/rad,
%   C{2} horang/rad,
%   C{3} ver/rad,
%   C{4} verang/rad
%
%see also:

warning('off','all');

%% parse inputs
p = inputParser();
addRequired(p,'obj');
addRequired(p,'PERTURB',@isnumeric);
addRequired(p,'PVALUE',@isnumeric);
addRequired(p,'FIELD',@ischar);
addRequired(p,'M',@isnumeric);
addRequired(p,'N',@isnumeric);
addOptional(p,'K',[0 0],@isnumeric);
addOptional(p,'CV',[0 0],@isnumeric);
addOptional(p,'Septa',[0 0],@isnumeric);

parse(p,obj,PERTURB,PVALUE,FIELD,M,N,varargin{:});

K = p.Results.K;
V = p.Results.CV;
H = p.Results.Septa;

O = length(obj.indBPM);
P = length(PERTURB);
C = [];%{zeros(O,P),zeros(O,P),zeros(O,P),zeros(O,P)};


if length(PVALUE) ~= P
    PVALUE = PVALUE(ones(1,P(1)));
end

RING = obj.rmodel;

orbit_function_handle = @(RING)obj.findNturnstrajectory6Err(RING,K,V,H);

% ORBIT = orbit_function_handle(RING);

mn = {M,N};
for i = 1:P
    
    oldvalue = getfield(RING{PERTURB(i)},FIELD,mn);
    RING{PERTURB(i)} = setfield(RING{PERTURB(i)},FIELD,mn,oldvalue+PVALUE(i));
    ORBITPLUS  = orbit_function_handle(RING);
    RING{PERTURB(i)} = setfield(RING{PERTURB(i)},FIELD,mn,oldvalue-PVALUE(i));
    ORBITMINUS  = orbit_function_handle(RING);
    RING{PERTURB(i)} = setfield(RING{PERTURB(i)},FIELD,mn,oldvalue);
    DORBIT = (ORBITPLUS - ORBITMINUS)/2;
    if any(isnan(DORBIT))
        warning(find(isnan(DORBIT)))
    end
    %ORBITMINU  = feval(orbit_function_handle,RING,orbit_function_args{:});
    %RING{PERTURB(i)} = setfield(RING{PERTURB(i)},varargin{1},mn,oldvalue);
    %DORBIT = (ORBITPLUS - ORBITMINU)/2;
    C{1}(:,i) = DORBIT(1,:);
    C{2}(:,i) = DORBIT(2,:);
    C{3}(:,i) = DORBIT(3,:);
    C{4}(:,i) = DORBIT(4,:);
end

warning('on','all');

