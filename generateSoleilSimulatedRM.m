ft = FirstTurns('soleil');
ft.setTurns(20); % set turns to compute trajetory RM
ft.injoff = -6e-3; % set the injected - stored beam offset.
TrajRM = ft.SimulateTrajectoryRM('RF',true,'rad',true); % simulate trajectory RM
ModelRM = ft.ModelRM; % display computed trajectory RMs

save('/mntdirect/_machfs/carver/firstTurnsSoleil/first-turns-steering/@RingControl/SOLEIL_SimulatedRM.mat', 'ModelRM');