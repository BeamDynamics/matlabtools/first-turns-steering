%% test movable for h streerers
sh      = movable('HCOR',...
    'get_fun',@()getam('HCOR','Physics'),...
    'set_fun',@(values)setsp('HCOR',values',obj.indHst','Physics'),...
    'absolute',true,'limits',[-0.980e-3 0.980e-3]);

% test get: this should return the array of steerers value
sh.get

%% test movable for v streerers
sv      = movable('VCOR',...
    'get_fun',@()getam('VCOR','Physics'),...
    'set_fun',@(values)setsp('VCOR',values',obj.indVst','Physics'),...
    'absolute',true,'limits',[-.711e-3 .711e-3]);

% test get: this should return the array of steerers value
sv.get


%% test FirstTurns class
FT = FirstTurns('soleil');

%read trajectory
FT.measuretrajectory

